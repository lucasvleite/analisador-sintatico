package sintatico;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import lexico.AnaliseLexica;
import lexico.Dado;

/**
 *
 * @author
 *  Breno Ramos
 *  Lucas Vicente
 *  Marina Alcântara
 */
public class Sintatico {

    public static ArrayList<Dado> tabelaTokensCertos = new ArrayList();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFileChooser fc = new JFileChooser();
        if (fc.showOpenDialog(fc) == 0) {
            try (FileReader ponteiroArquivo = new FileReader(fc.getSelectedFile().getAbsolutePath())) {

                try (BufferedReader codigo = new BufferedReader(ponteiroArquivo);) {
                    new AnaliseLexica(codigo, tabelaTokensCertos);
                    new AnaliseSintatica(tabelaTokensCertos);
                }

            } catch (IOException ex) {
                System.out.print(ex);
            }
        }
    }
}
