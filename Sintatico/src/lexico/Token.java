package lexico;

/**
 *
 * @author Breno Ramos e Lucas Vicente
 */
public class Token {
    private String tipo;
    private String valor;
    private final String stringToken;

    
    public Token(String tipo, String valor) {
        this.tipo = tipo;
        this.valor = valor;
        this.stringToken = "< "+tipo+" , "+valor+" >";
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
    public String getStringToken(){
        return stringToken;
    }
}