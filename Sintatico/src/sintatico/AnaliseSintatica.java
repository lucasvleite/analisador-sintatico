package sintatico;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import lexico.Dado;
import lexico.Token;

/**
 *
 * @author
 *  Breno Ramos
 *  Lucas Vicente
 *  Marina Alcântara
 */
public class AnaliseSintatica {

    private final ArrayList<Dado> listaTokens;
    int posicaoToken = -1;

    static final String[] MODIFIERS = { "public", "protected", "private", "static", "abstract" };
    static final String[] BASICTYPE = { "boolean", "char", "int" };

    AnaliseSintatica(ArrayList<Dado> tabelaTokens) {
        this.listaTokens = tabelaTokens;
        if (AnaliseSintatica())
            JOptionPane.showMessageDialog(null, "Executado com sucesso!", "resultado Analise Sintatica",INFORMATION_MESSAGE);
        else
            JOptionPane.showMessageDialog(null, "Foi encontrado problemas para executar. Tente Novamente!", "resultado Analise Sintatica", ERROR_MESSAGE);
    }

    Token la(int pegarToken) {
        if (posicaoToken + pegarToken < listaTokens.size()) {
            return listaTokens.get(posicaoToken + pegarToken).getToken();
        }
        return null;
    }

    public void match(Token token) {
        posicaoToken += 1;
    }

    boolean isModifiers(Token token) {
        for (String aux : MODIFIERS) {
            if (aux.equals(token.getValor())) {
                return true;
            }
        }
        return false;
    }

    boolean isBasicType(Token token) {
        for (String aux : BASICTYPE) {
            if (aux.equals(token.getValor())) {
                return true;
            }
        }
        return false;
    }

    boolean isLiteral(Token token) {
        return ("Integer".equals(token.getTipo()) || "Character".equals(token.getTipo())
                || "String".equals(token.getTipo()) || "true".equals(la(1).getValor())
                || "false".equals(la(1).getValor()) || "null".equals(la(1).getValor()));
    }

    boolean firstPrimary(Token token) {
        return ("(".equals(token.getValor()) || "this".equals(token.getValor()) || "super".equals(token.getValor())
                || isLiteral(token) || "new".equals(token.getValor()) || "id".equals(token.getTipo()));
    }

    boolean firstSimpleUnaryExpression(Token token) {
        return ("!".equals(token.getValor()) || firstPrimary(token));
    }

    boolean firstExpression(Token token) {
        return ("++".equals(token.getValor()) || "-".equals(token.getValor()) || firstSimpleUnaryExpression(token));
    }
    
    public void VerificarLocalOrStatement(){
        if ("id".equals(la(1).getTipo())) {
            match(la(1));
            while (".".equals(la(1).getValor()) && "id".equals(la(2).getTipo())) {
                match(la(1));
                match(la(1));
            }
        }
    }

    public void compilationUnit() {
        if (la(1).getValor().equals("package")) {
            match(la(1));
            qualifiedIdentifier();
            if (la(1).getValor().equals(";")) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- compilationUnit");
            }
        }
        while ("import".equals(la(1).getValor())) {
            match(la(1));
            qualifiedIdentifier();
            if (la(1).getValor().equals(";")) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- compilationUnit");
            }
        }
        while (posicaoToken < listaTokens.size() - 1) {
            if (isModifiers(la(1))) {
                typeDeclaration();
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- compilationUnit");
            }
        }
    }

    public void qualifiedIdentifier() {
        if ("id".equals(la(1).getTipo())) {
            match(la(1));
            while (".".equals(la(1).getValor()) && "id".equals(la(2).getTipo())) {
                match(la(1));
                match(la(1));
            }
            if (".".equals(la(1).getValor())) {
                match(la(1));
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- qualifiedIdentifier");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- qualifiedIdentifier");
        }
    }

    public void typeDeclaration() {
        modifiers();
        classDeclaration();
    }

    public void modifiers() {
        if (isModifiers(la(1))) {
            match(la(1));
            while (isModifiers(la(1))) {
                match(la(1));
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- ");
        }
    }

    public void classDeclaration() {
        if ("class".equals(la(1).getValor())) {
            match(la(1));
            if ("id".equals(la(1).getTipo())) {
                match(la(1));
                if ("extends".equals(la(1).getValor())) {
                    match(la(1));
                    qualifiedIdentifier();
                }
                classBody();
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- ");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- ");
        }
    }

    public void classBody() {
        if ("{".equals(la(1).getValor())) {
            match(la(1));
            while (isModifiers(la(1))) {
                modifiers();
                memberDecl();
            }
            if ("}".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- ");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- ");
        }
    }

    public void memberDecl() {
        if (("void").equals(la(1).getValor())) {
            match(la(1));
            if ("id".equals(la(1).getTipo())) {
                match(la(1));
                formalParameters();
                while (";".equals(la(1).getValor()) || "{".equals(la(1).getValor())) {
                    if (";".equals(la(1).getValor())) {
                        match(la(1));
                    } else if ("{".equals(la(1).getValor())) {
                        block();
                    }
                }
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- memberDecl");
            }
        } else if ("id".equals(la(1).getTipo()) && "(".equals(la(2).getValor())) {
            match(la(1));
            formalParameters();
            block();
        } else if ("id".equals(la(1).getTipo()) || isBasicType(la(1))) {
            type();
            if ("id".equals(la(1).getTipo())) {
                if ("(".equals(la(2).getValor())) {
                    match(la(1));
                    formalParameters();
                    if (";".equals(la(1).getValor())) {
                        match(la(1));
                    }
                    else if ("{".equals(la(1).getValor())) {
                        block();
                    }
                    else {
                        match(la(1));
                        new Imprimir(listaTokens.get(posicaoToken),"- memberDecl2");
                    }
                } else {
                    variableDeclarators();
                    if (";".equals(la(1).getValor())) {
                        match(la(1));
                    } else {
                        match(la(1));
                        new Imprimir(listaTokens.get(posicaoToken),"- memberDecl2");
                    }
                }
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- memberDecl3");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- memberDecl4");
        }
    }

    public void block() {
        if ("{".equals(la(1).getValor())) {
            match(la(1));
            while ("{".equals(la(1).getValor()) || "id".equals(la(1).getTipo()) || "if".equals(la(1).getValor())
                    || "while".equals(la(1).getValor()) || "return".equals(la(1).getValor())
                    || ";".equals(la(1).getValor()) || firstExpression(la(1)) || isBasicType(la(1))) {
                blockStatement();
            }

            if ("}".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- block");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- block2");
        }
    }

    public void blockStatement() {
        if(isBasicType(la(1))
        || ("id".equals(la(1).getTipo()) && "[".equals(la(2).getValor()))
        || ("id".equals(la(1).getTipo()) && ".".equals(la(2).getValor()))
        || ("id".equals(la(1).getTipo()) && "id".equals(la(2).getTipo()))) {
            int rollBack = posicaoToken;
            VerificarLocalOrStatement();
            if("(".equals(la(1).getValor())){
                posicaoToken = rollBack;
                statement();
            }
            else {
                posicaoToken = rollBack;
                localVariableDeclarationStatement();
            }
        }
        else if ("{".equals(la(1).getValor()) || ("id".equals(la(1).getTipo()) && ":".equals(la(2).getValor()))
        || "if".equals(la(1).getValor()) || "while".equals(la(1).getValor()) || "return".equals(la(1).getValor())
        || ";".equals(la(1).getValor()) || firstExpression(la(1))) {
            statement();
        }
        else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- blockStatement");
        }
    }

    public void statement() {
        if ("{".equals(la(1).getValor())) {
            block();
        }
        else if ("id".equals(la(1).getTipo()) && ":".equals(la(1).getValor())) {
            match(la(1));
            match(la(1));    
            statement();
        }
        else if ("if".equals(la(1).getValor())) {
            match(la(1));
            parExpression();
            statement();
            if ("else".equals(la(1).getValor())) {
                match(la(1));
                statement();
            }
        }
        else if ("while".equals(la(1).getValor())) {
            match(la(1));
            parExpression();
            statement();
        }
        else if ("return".equals(la(1).getValor())) {
            match(la(1));

            if(firstExpression(la(1))) {
                expression();
            }
            if (";".equals(la(1).getValor())) {
                match(la(1));
            }
            else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- statement3");
            }
        }
        else if (";".equals(la(1).getValor())) {
            match(la(1));
        }
        else {
            statementExpression();
            if (";".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- statement4");
            }
        }
    }

    public void formalParameters() {
        if ("(".equals(la(1).getValor())) {
            match(la(1));
            if ("id".equals(la(1).getTipo()) || isBasicType(la(1))) {
                formalParameter();
                while (",".equals(la(1).getValor())) {
                    match(la(1));
                    formalParameter();
                }
            }
            if(")".equals(la(1).getValor())){
                match(la(1));
            }
            else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- formalParameters");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- formalParameters2");
        }
    }

    public void formalParameter() {
        if ("id".equals(la(1).getTipo()) || isBasicType(la(1))) {
            type();
            if ("id".equals(la(1).getTipo())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- formalParameter");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- formalParameter2");
        }
    }

    public void parExpression() {
        if ("(".equals(la(1).getValor())) {
            match(la(1));
            expression();
            if (")".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- parExpression");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- parExpression");
        }
    }

    public void localVariableDeclarationStatement() {
        if ("id".equals(la(1).getTipo()) || isBasicType(la(1))) {
            type();
            variableDeclarators();
            if (";".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- localVariableDeclarationStatement");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- localVariableDeclarationStatement2");
        }
    }

    public void variableDeclarators() {
        if ("id".equals(la(1).getTipo())) {
            variableDeclarator();
            while (",".equals(la(1).getValor())) {
                match(la(1));
                if ("id".equals(la(1).getTipo())) {
                    variableDeclarator();
                } else {
                    match(la(1));
                    new Imprimir(listaTokens.get(posicaoToken),"- variableDeclarators");
                }
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- variableDeclarators2");
        }
    }

    public void variableDeclarator() {
        if ("id".equals(la(1).getTipo())) {
            match(la(1));
            while ("=".equals(la(1).getValor())) {
                match(la(1));
                variableInitializer();
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- variableDeclarator");
        }
    }

    public void variableInitializer() {
        if ("{".equals(la(1).getValor())) {
            arrayInitializer();
        } else if (firstExpression(la(1))) {
            expression();
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- variableInitializer");
        }
    }

    public void arrayInitializer() {
        if ("{".equals(la(1).getValor())) {
            match(la(1));

            if (firstExpression(la(1)) || "{".equals(la(1).getValor())) {
                variableInitializer();
                while (",".equals(la(1).getValor())) {
                    match(la(1));
                    variableInitializer();
                }
            }

            if ("}".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- arrayInitializer");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- arrayInitializer");
        }
    }

    public void arguments() {
        if ("(".equals(la(1).getValor())) {
            match(la(1));

            if (firstExpression(la(1))) {
                expression();
                while (",".equals(la(1).getValor())) {
                    match(la(1));
                    expression();
                }
            }

            if (")".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- arguments");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- arguments2");
        }
    }

    public void type() {
        if ("id".equals(la(1).getTipo())) {
            referenceType();
        }
        else if (isBasicType(la(1))) {
            if ("[".equals(la(2).getValor())) {
                referenceType();
            } else {
                match(la(1));
            }
        }
        else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- type");
        }
    }

    public void basicType() {
        if (isBasicType(la(1))) {
            match(la(1));
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- type");
        }
    }

    public void referenceType() {
        if (isBasicType(la(1))) {
            basicType();
            if ("[".equals(la(1).getValor())) {
                match(la(1));
                if ("]".equals(la(1).getValor())) {
                    match(la(1));
                    while ("[".equals(la(1).getValor()) && "]".equals(la(2).getValor())) {
                        match(la(1));
                        match(la(1));
                    }
                    if ("[".equals(la(1).getValor())) {
                        match(la(1));
                        match(la(1));
                        new Imprimir(listaTokens.get(posicaoToken),"- referenceType");
                    }
                } else {
                    match(la(1));
                    new Imprimir(listaTokens.get(posicaoToken),"- referenceType2");
                }
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- referenceType3");
            }
        } else if ("id".equals(la(1).getTipo())) {
            qualifiedIdentifier();
            while ("[".equals(la(1).getValor()) && "]".equals(la(2).getValor())) {
                match(la(1));
                match(la(1));
            }
            if ("[".equals(la(1).getValor())) {
                match(la(1));
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- referenceType4");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- referenceType5");
        }
    }

    public void statementExpression() {
        expression();
    }

    public void expression() {
        assignmentExpression();
    }

    public void assignmentExpression() {
        conditionalAndExpression();
        if ("=".equals(la(1).getValor()) || "+=".equals(la(1).getValor())) {
            match(la(1));
            assignmentExpression();
        }
    }

    public void conditionalAndExpression() {
        equalityExpression();
        while ("&&".equals(la(1).getValor())) {
            match(la(1));
            equalityExpression();
        }
    }

    public void equalityExpression() {
        relationalExpression();
        while ("==".equals(la(1).getValor())) {
            match(la(1));
            relationalExpression();
        }
    }

    public void relationalExpression() {
        additiveExpression();

        if (">".equals(la(1).getValor()) || "<=".equals(la(1).getValor())) {
            match(la(1));
            additiveExpression();
        } else if ("instanceof".equals(la(1).getValor())) {
            match(la(1));
            referenceType();
        }
    }

    public void additiveExpression() {
        multiplicativeExpression();
        while ("+".equals(la(1).getValor()) || "-".equals(la(1).getValor())) {
            match(la(1));
            multiplicativeExpression();
        }
    }

    public void multiplicativeExpression() {
        unaryExpression();
        while ("*".equals(la(1).getValor())) {
            match(la(1));
            unaryExpression();
        }
    }

    public void unaryExpression() {
        if ("++".equals(la(1).getValor())) {
            match(la(1));
            unaryExpression();
        } else if ("-".equals(la(1).getValor())) {
            match(la(1));
            unaryExpression();
        }
        else if(firstSimpleUnaryExpression(la(1))) {
            simpleUnaryExpression();
        }
        else{
            new Imprimir(listaTokens.get(posicaoToken),"- unaryExpression");
        }
    }

    public void simpleUnaryExpression() {
        if ("!".equals(la(1).getValor())) {
            match(la(1));
            unaryExpression();
        } else if ("(".equals(la(1).getValor())) {
            match(la(1));
            if (isBasicType(la(1))) {
                if (")".equals(la(2).getValor())) {
                    basicType();
                    match(la(1));
                } else {
                    referenceType();
                    if (")".equals(la(1).getValor())) {
                        match(la(1));
                        unaryExpression();
                    } else {
                        match(la(1));
                        new Imprimir(listaTokens.get(posicaoToken),"- simpleUnaryExpression");
                    }
                }
            } else if ("id".equals(la(1).getTipo())) {
                referenceType();
                if (")".equals(la(1).getValor())) {
                    match(la(1));
                    unaryExpression();
                } else {
                    match(la(1));
                    new Imprimir(listaTokens.get(posicaoToken),"- simpleUnaryExpression");
                }
            }
        } else if (firstPrimary(la(1))) {
            postfixExpression();
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- simpleUnaryExpression");
        }

    }

    public void postfixExpression() {
        primary();
        while (".".equals(la(1).getValor()) || "[".equals(la(1).getValor())) {
            selector();
        }
        while ("--".equals(la(1).getValor())) {
            match(la(1));
        }
    }

    public void selector() {
        if (".".equals(la(1).getValor())) {
            match(la(1));
            if ("id".equals(la(1).getTipo())) {
                qualifiedIdentifier();
                if ("(".equals(la(1).getValor())) {
                    arguments();
                }
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- selector");
            }
        } else if ("[".equals(la(1).getValor())) {
            match(la(1));
            expression();
            if ("]".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- selector");
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- selector");
        }
    }

    public void primary() {
        if ("(".equals(la(1).getValor())) {
            parExpression();
        } else if ("this".equals(la(1).getValor())) {
            match(la(1));
            if ("(".equals(la(1).getValor())) {
                arguments();
            }
        } else if ("super".equals(la(1).getValor())) {
            match(la(1));
            if ("(".equals(la(1).getValor())) {
                arguments();
            } else if (".".equals(la(1).getValor())) {
                match(la(1));
                if ("id".equals(la(1).getTipo())) {
                    match(la(1));
                    if ("(".equals(la(1).getValor())) {
                        arguments();
                    }
                }
            } else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- primary");
            }
        } else if (isLiteral(la(1))) {
            literal();
        } else if ("new".equals(la(1).getValor())) {
            match(la(1));
            creator();
        } else if ("id".equals(la(1).getTipo())) {
            qualifiedIdentifier();
            if ("(".equals(la(1).getValor())) {
                arguments();
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- primary");
        }
    }

    public void creator() {
        if (isBasicType(la(1)) || "id".equals(la(1).getTipo())) {
            if ("id".equals(la(1).getTipo())) {
                qualifiedIdentifier();
            } else
                match(la(1));
            
            if ("(".equals(la(1).getValor())) {
                arguments();
            }
            else if("[".equals(la(1).getValor()) && firstExpression(la(2))){
                newArrayDeclarator();
            }
            else {
                if("[".equals(la(1).getValor())) {
                    match(la(1));
                    if("]".equals(la(1).getValor())) {
                        match(la(1));
                        while("[".equals(la(1).getValor())) {
                            match(la(1));
                            if("]".equals(la(1).getValor())) {
                                match(la(1));
                            }
                            else {
                                match(la(1));
                                new Imprimir(listaTokens.get(posicaoToken),"- creator");
                            }
                        }
                        if("{".equals(la(1).getValor())) {
                            arrayInitializer();
                        }
                    }
                    else {
                        match(la(1));
                        new Imprimir(listaTokens.get(posicaoToken),"- creator");
                    }
                }
                else {
                    match(la(1));
                    new Imprimir(listaTokens.get(posicaoToken),"- creator");
                }
            }
        } else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- creator");
        }
    }

    public void newArrayDeclarator() {
        if ("[".equals(la(1).getValor())) {
            match(la(1));
            expression();
            if ("]".equals(la(1).getValor())) {
                match(la(1));
                while ("[".equals(la(1).getValor()) && firstExpression(la(2))) {
                    match(la(1));
                    expression();
                    if ("]".equals(la(1).getValor())) {
                        match(la(1));
                    }
                    else {
                        match(la(1));
                        new Imprimir(listaTokens.get(posicaoToken),"- newArrayDeclarator");
                    }
                }
                while ("[".equals(la(1).getValor())) {
                    match(la(1));
                    if ("]".equals(la(1).getValor())) {
                        match(la(1));
                    }
                    else {
                        match(la(1));
                        new Imprimir(listaTokens.get(posicaoToken),"- newArrayDeclarator");
                    }
                }
            }
            else {
                match(la(1));
                new Imprimir(listaTokens.get(posicaoToken),"- newArrayDeclarator");
            }
        }
        else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- newArrayDeclarator");
        }
    }

    public void literal() {
        if (isLiteral(la(1)))
            match(la(1));
        else {
            match(la(1));
            new Imprimir(listaTokens.get(posicaoToken),"- literal");
        }
    }

    /*
     *
     */
    private boolean AnaliseSintatica() {
        compilationUnit();

        new Imprimir();
        return true;
    }
}
